export const renderGlassesList = (men) => {
    let contentHTML = "";
  
    men.forEach((index) => {
      contentHTML += `
      <a class="col-4" >
          <img onclick = showThongTin('${index.id}') src="${index.src}" alt="" class="img-fluid" >
      </a>`;
    });
  
    document.getElementById("vglassesList").innerHTML = contentHTML;
  };

export function timKimViTri(id, tan) {
  for (var index = 0; index < tan.length; index++) {
    var hinh = tan[index];
    if (hinh.id == id) {
      return index;
    }
  }
  return -1;
};

export const renderGlassesInfo = (glass) => {
  return `
  <img src=${glass.virtualImg}></img>
  <a>
    <h4>${glass.name} - ${glass.brand} (${glass.color})</h4>
    <span>${glass.price}$</span>
    <p>${glass.description}</p>
    </a>
     `;
};
  